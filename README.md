# Formica

[![npm](https://img.shields.io/npm/v/@shelacek/formica.svg)](https://www.npmjs.com/package/@shelacek/formica)
[![npm bundle size (minified + gzip)](https://img.shields.io/bundlephobia/minzip/@shelacek/formica.svg)](https://bundlephobia.com/result?p=@shelacek/formica)

*Preact forms made easy!*

See documentation on [shelacek.bitbucket.io/formica](https://shelacek.bitbucket.io/formica).

```jsx
<Form value={form} onChange={this.handleChange} onSubmit={this.handleSubmit}>
	<label>Name: <input name="name" type="text" required /></label>
	<FormGroup name="info">
		<label>Bio: <textarea name="bio" maxLength="250" /></label>
		<label>Website: <input name="web" type="text" pattern="(http://|https://)\S{1,63}" /></label>
	</FormGroup>
	<button type="submit">Submit form</button>
</Form>
```

## Features

- Close to native: write forms like you do in plain HTML
- Reactive: the data in the form are those that are in the component state and vice versa
- Freedom: you can directly use native inputs, create custom inputs or incorporate third party ones
- Use this.state, hooks or signals, class components or functional components. Your project, your rules!
- Light as a feather: only ~2.2kiB mingzipped and without dependencies (except Preact)


[Changelog] | [Create an issue]

[Changelog]: https://bitbucket.org/shelacek/formica/src/master/CHANGELOG.md
[Create an issue]: https://bitbucket.org/shelacek/formica/issues
