# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](http://keepachangelog.com/en/1.0.0/)
and this project adheres to [Semantic Versioning](http://semver.org/spec/v2.0.0.html).


## [Unreleased]

There are no changes yet.


## [2.0.2] - 2025-01-30

### Fixed

- Added missing types export to package.json.


## [2.0.1] - 2024-09-04

### Changed

- Dev dependences have been updated, but there should be no functional changes in the build.


## [2.0.0] - 2022-10-09

### Changed

- Simplify README and added link to documentation.


## [2.0.0-alpha.6] - 2022-09-25

### Changed

- `FormControl` auto-map label and input if they not already have id/for attribute. This
  reintoduce feature removed in [0.3.0], but with more control.


## [2.0.0-alpha.5] - 2020-08-17

### Fixed

- Fix regression on linking native `input[type=number]` or `input[type=radio]` in `FormGroup`.
  Sorry about that.


## [2.0.0-alpha.4] - 2020-08-17

### Fixed

- Fix issue, when `input[type=number]` with non-numeric content gets cleared and trailing decimal
  mark is removed.


## [2.0.0-alpha.3] - 2020-07-24

### Fixed

- Fix issue when some nested elements are not rendered.


## [2.0.0-alpha.2] - 2020-07-24

### Added

- Any extra prop (except `children`, `value`, `onChange`, and `onSubmit`) on `Form` is now applied
 to underlying `<form>` element.

### Changed

- **BREAKING**: Fieldsets are removed from `FormGroup` and `FormArray`, so these component now
  not implicitly creates any DOM elements. If you depend on fieldsets, please introduce them
  manually.


## [2.0.0-alpha.1] - 2020-07-24

### Changed

- **BREAKING**: Preact 10 (aka PreactX) is now supported :-). Support for Preact 8
  is dropped :-(.


## [1.5.2] - 2020-08-17

### Fixed

- Fix issue, when `input[type=number]` with non-numeric content gets cleared and trailing decimal
  mark is removed.


## [1.5.1] - 2020-07-07

### Changed

- Remove all `shouldComponentUpdate` - more harm than good.


## [1.5.0] - 2020-04-09

### Added

- `FormAction` remove action can be called on entire array to remove last element.


## [1.4.0] - 2019-09-17

### Added

- `FormAction.item` now accepts a function as a builder function for the item value.


## [1.3.0] - 2019-07-30

### Added

- Added new helper component `FormAction` to allow simple form mutations.

### Fixed

- `onChange` passed to function as child is now handled by `FormGroup`.
- Fixed `asFormControl` HOC typing.

### Changed

- `onChange` events with undefined value now remove associated item from form model.

### Deprecated

- Upcoming version `2.0.0` doesn't have `<fieldset>`, so old way of adding/removing form array
  items is now deprecated. Use `FormAction` instead.

### Fixed

- `onChange` passed to function as child is now handled by `FormGroup`.


## [1.2.0] - 2019-04-13

### Added

- Flow typings.
- `FormGroup` now accepts render prop - children() callback.

### Fixed

- Fix TS typings (added `class` and `keyedBy` props).
- Remove invalid style links from `package.json`.


## [1.1.1] - 2018-10-17

### Fixed

- Fix handling of `input[type="radio"]`.


## [1.1.0] - 2018-10-11

### Added

- Added `class` alias for `className` prop. These two are now equivalent.
- Added `keyedBy` prop to enable keying of `FormArray`'s items.


## [1.0.0] - 2018-09-25

### Fixed

- Added `id` prop to `FormControl` typings.
- Fix `<legend>` detection in `FormGroup`.
- Fix linking of native control's events.


## [0.3.0] - 2018-09-21

### Changed

- **BREAKING**: `FormControl` no longer auto-map label with corresponding
  `input`/`textarea`/`select`. `FormControl` has a new `id` prop, that is mapped to control's `id`
  attribute and label `for` attribute.


## [0.2.4] - 2018-09-21

### Changed

- Revert incompatible changes introduced in 0.2.3, that break SEMVER. Sorry for this.


## [0.2.3] - 2018-09-21

### Added

- `FormGroup` don't add `[role="none"]` if there is a `<legend>` child.


### Changed

- **BREAKING**: `FormControl` no longer auto-map label with corresponding
  `input`/`textarea`/`select`. `FormControl` has a new `id` prop, that is mapped to control's `id`
  attribute and label `for` attribute.


### Fixed

- Fix disabled and validity attributes of `FormControl`'s render callback and classes. Browser
  according to expectations needs input in correct state before read the `ValidityState`, but
  (P)react update parents after it's children, so `ValidityState` inevitably represents old state.
  Fix moves `disabled` attributes to controls and updates them already on calling `render()`.
  It's not very nice, but it's workig.


## [0.2.2] - 2018-08-29

### Added

- `FormControl` now expose `invalid` CSS class.


### Fixed

- Fix error where empty or zero-value fields without name not render
  (e.g. fields directly in `FormArray`).
- Fix handling values of native fields (like `HTMLInputElement.valueAsNumber`).


## [0.2.1] - 2018-08-27

### Fixed

- Consider `input[type="number"]` and `input[type="range"]` as numeric input. This fix faulty
  converting model value to string.


## [0.2.0] - 2018-08-27

### Changed

- **BREAKING**: Distinguish controls on `asFormControl` HOC instead of `name` prop.
  This change enables use `name`-less controls, like array-of-value controls and avoid collision
  with another components with `name` prop.

    Please enhance your custom controls with `asFormControl` HOC.


## [0.1.1] - 2018-08-27

### Fixed

- Re-render on disabled prop change (fix `shouldComponentUpdate`).


## [0.1.0] - 2018-08-25

### Added

- Initial version.


[Unreleased]: https://bitbucket.org/shelacek/formica/branches/compare/master..v2.0.2
[2.0.2]: https://bitbucket.org/shelacek/formica/branches/compare/v2.0.2..v2.0.1
[2.0.1]: https://bitbucket.org/shelacek/formica/branches/compare/v2.0.1..v2.0.0
[2.0.0]: https://bitbucket.org/shelacek/formica/branches/compare/v2.0.0..v2.0.0-alpha.6
[2.0.0-alpha.6]: https://bitbucket.org/shelacek/formica/branches/compare/v2.0.0-alpha.6..v2.0.0-alpha.5
[2.0.0-alpha.5]: https://bitbucket.org/shelacek/formica/branches/compare/v2.0.0-alpha.5..v2.0.0-alpha.4
[2.0.0-alpha.4]: https://bitbucket.org/shelacek/formica/branches/compare/v2.0.0-alpha.4..v2.0.0-alpha.3
[2.0.0-alpha.3]: https://bitbucket.org/shelacek/formica/branches/compare/v2.0.0-alpha.3..v2.0.0-alpha.2
[2.0.0-alpha.2]: https://bitbucket.org/shelacek/formica/branches/compare/v2.0.0-alpha.2..v2.0.0-alpha.1
[2.0.0-alpha.1]: https://bitbucket.org/shelacek/formica/branches/compare/v2.0.0-alpha.1..v1.5.1
[1.5.2]: https://bitbucket.org/shelacek/formica/branches/compare/v1.5.2..v1.5.1
[1.5.1]: https://bitbucket.org/shelacek/formica/branches/compare/v1.5.1..v1.5.0
[1.5.0]: https://bitbucket.org/shelacek/formica/branches/compare/v1.5.0..v1.4.0
[1.4.0]: https://bitbucket.org/shelacek/formica/branches/compare/v1.4.0..v1.3.0
[1.3.0]: https://bitbucket.org/shelacek/formica/branches/compare/v1.3.0..v1.2.0
[1.2.0]: https://bitbucket.org/shelacek/formica/branches/compare/v1.2.0..v1.1.1
[1.1.1]: https://bitbucket.org/shelacek/formica/branches/compare/v1.1.1..v1.1.0
[1.1.0]: https://bitbucket.org/shelacek/formica/branches/compare/v1.1.0..v1.0.0
[1.0.0]: https://bitbucket.org/shelacek/formica/branches/compare/v1.0.0..v0.3.0
[0.3.0]: https://bitbucket.org/shelacek/formica/branches/compare/v0.3.0..v0.2.4
[0.2.4]: https://bitbucket.org/shelacek/formica/branches/compare/v0.2.4..v0.2.3
[0.2.3]: https://bitbucket.org/shelacek/formica/branches/compare/v0.2.3..v0.2.2
[0.2.2]: https://bitbucket.org/shelacek/formica/branches/compare/v0.2.2..v0.2.1
[0.2.1]: https://bitbucket.org/shelacek/formica/branches/compare/v0.2.1..v0.2.0
[0.2.0]: https://bitbucket.org/shelacek/formica/branches/compare/v0.2.0..v0.1.1
[0.1.1]: https://bitbucket.org/shelacek/formica/branches/compare/v0.1.1..v0.1.0
[0.1.0]: https://bitbucket.org/shelacek/formica/commits/tag/v0.1.0
