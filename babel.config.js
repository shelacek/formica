export default function(api) {
	api.cache(true);
	return {
		presets: [["@babel/preset-env", {
			"modules": false,
			"loose": true,
			"shippedProposals": true,
			"targets": { "browsers": ["> 1%", "not ie > 0", "not dead"] }
		}]],
		plugins: [
			["@babel/proposal-decorators", { "legacy": true }],
			["@babel/proposal-class-properties"],
			["@babel/transform-react-jsx", { "pragma": "h", "useBuiltIns": true }]
		]
	};
}
