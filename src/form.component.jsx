import { h, Component } from 'preact';
import { bind } from './utils/bind';
import { FormGroup } from './controls/form-group.component';

export class Form extends Component {
	@bind
	_handleChange(event) {
		this.props.onChange && this.props.onChange(event.target.value);
	}

	@bind
	_handleSubmit(event) {
		this.props.onSubmit && this.props.onSubmit(event);
		event.preventDefault();
	}

	render({ children, value, onChange, onSubmit, ...attrs }) {
		return (
			<form onSubmit={this._handleSubmit} noValidate {...attrs}>
				<FormGroup value={value} onChange={this._handleChange}>
					{children}
				</FormGroup>
			</form>
		);
	}
}
