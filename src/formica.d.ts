import { Component, ComponentFactory, ComponentChild } from 'preact';

export declare function asFormControl<P>(WrappedControl: ComponentFactory<P>): ComponentFactory<P>;

export type OnChangeEvent<T> = Event | {
	target: {
		name: string | number;
		value: T;
	}
};

export type OnChangeHandler<T> = { (event: OnChangeEvent<T>): void };

export interface ControlAttributes<T> {
	class?: string;
	className?: string;
	disabled?: boolean;
	name: string | number;
	value: T;
	onChange: OnChangeHandler<T>;
}

export interface FormProps {
	value?: any;
	onChange?: { (value: any): void };
	onSubmit?: { (event: Event): void };
	[attrs: string]: any;
}

export declare class Form extends Component<FormProps> {
	render(): ComponentChild;
}

export interface FormGroupProps extends Partial<ControlAttributes<any>> { }

export declare class FormGroup extends Component<FormGroupProps> {
	render(): ComponentChild;
}

export interface FormArrayProps extends Partial<ControlAttributes<any[]>> {
	keyedBy?: string;
}

export declare class FormArray extends Component<FormArrayProps> {
	render(): ComponentChild;
}

export interface FormControlProps extends Partial<ControlAttributes<any>> {
	id?: string;
}

export declare class FormControl extends Component<FormControlProps> {
	render(): ComponentChild;
}

export interface FormActionRenderPropParams {
	add?: { (): void };
	remove?: { (): void };
}

export interface FormActionProps {
	children?: { (params?: FormActionRenderPropParams): ComponentChild };
	disabled?: boolean;
	name?: string | number;
	value?: any;
	onChange?: OnChangeHandler<any>;
	item?: any
}

export declare class FormAction extends Component<FormActionProps> {
	render(): ComponentChild;
}
