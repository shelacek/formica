import { h, Component } from 'preact';
import { FORM_CONTROL_SYMBOL } from './internal/form-control-symbol';

export function asFormControl(WrappedControl) {
	return class extends Component {
		static type = FORM_CONTROL_SYMBOL;

		render(props) {
			return <WrappedControl {...props} />;
		}
	};
}
