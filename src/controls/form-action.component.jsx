import { toChildArray, Component } from 'preact';
import { bind } from '../utils/bind';
import { asFormControl } from '../as-form-control';

export const FormAction = asFormControl(class extends Component {
	@bind
	_handleAdd() {
		const { item, name } = this.props;
		const object = typeof item === 'function' ? item() : item;
		const value = Array.isArray(this.props.value)
			? [...this.props.value, object]
			: { ...this.props.value, ...object };
		this.props.onChange({ target: { name, value } });
	}

	@bind
	_handleRemove() {
		const { name, value } = this.props;
		const items = Array.isArray(value)
			? value.slice(0, value.length - 1)
			: undefined;
		this.props.onChange({ target: { name: name, value: items } });
	}

	render({ children, ...attrs }) {
		const c = toChildArray(children);
		return c && typeof c[0] === 'function' && c[0]({
			add: this._handleAdd,
			remove: this._handleRemove,
			...attrs
		});
	}
});
