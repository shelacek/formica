import { h, toChildArray, Component } from 'preact';
import { bind } from '../utils/bind';
import { withProps } from '../utils/with-props';
import { controlValueToProp } from '../utils/control-value-to-prop';
import { asFormControl } from '../as-form-control';

let controlId = 0;

export const FormControl = asFormControl(class extends Component {
	_ref = null;
	_id = controlId++;

	state = {
		touched: false
	};

	_getControlAttrs(control) {
		return {
			disabled: this.props.disabled,
			...controlValueToProp(control, this.props.value)
		};
	}

	_applyAttrs(node, attrs) {
		for (const key in attrs) {
			if (Object.prototype.hasOwnProperty.call(attrs, key)
					&& !Object.is(node[key], attrs[key])) {
				node[key] = attrs[key];
			}
		}
	}

	@bind
	_handleBlur() {
		this.setState({ touched: true });
	}

	@bind
	_linker(vnode) {
		const control = vnode.props || {};
		const { id = `form-contol-${this._id}`, name, onChange } = this.props;
		switch (vnode && vnode.type) {
			case 'input':
			case 'textarea':
			case 'select':
				return [{
					id: vnode.props.id || id,
					ref: x => this._ref = x,
					[typeof name === 'string' ? 'name' : 'data-index']: name,
					onInput: onChange,
					onBlur: this._handleBlur,
					...this._getControlAttrs(control)
				}];
			case 'label':
				return !vnode.props.for && id && [
					{ htmlFor: id },
					withProps(vnode.props && vnode.props.children, this._linker)
				];
			default:
				return null;
		}
	}

	render({ children, className, disabled }, { touched }) {
		this._ref && this._applyAttrs(this._ref, this._getControlAttrs(this._ref));
		const validity = this._ref && this._ref.validity;
		const classNames = [
			this.props.class || className,
			disabled && 'disabled',
			touched && 'touched',
			validity && !validity.valid && 'invalid'
		];
		const c = toChildArray(children);
		const params = { disabled, touched, validity };
		const isRenderCallback = c && typeof c[0] === 'function';
		return (
			<div className={classNames.filter(x => x).join(' ')}>
				{withProps(isRenderCallback ? [c[0](params)] : c, this._linker)}
			</div>
		);
	}
});
