import { h, Component } from 'preact';
import { bind } from '../utils/bind';
import { FormGroup } from './form-group.component';
import { asFormControl } from '../as-form-control';

export const FormArray = asFormControl(class extends Component {
	@bind
	_handleChange(event) {
		const name = this.props.name;
		const value = this.props.value.slice();
		if (event.target.value === undefined) {
			value.splice(event.target.name, 1);
		}
		else {
			value.splice(event.target.name, 1, event.target.value);
		}
		this.props.onChange({ target: { name, value } });
	}

	render({ children, keyedBy, name, value, onChange, ...attrs }) {
		return value && value.map((x, index) => (
			<FormGroup
					key={keyedBy && x && x[keyedBy]}
					name={index}
					value={x}
					onChange={this._handleChange}
					{...attrs}>
				{children}
			</FormGroup>
		));
	}
});
