import { toChildArray, Component } from 'preact';
import { bind } from '../utils/bind';
import { withProps } from '../utils/with-props';
import { getControlValue } from '../utils/get-control-value';
import { getControlEvent } from '../utils/get-control-event';
import { controlValueToProp } from '../utils/control-value-to-prop';
import { isFormControl } from '../utils/is-form-control';
import { asFormControl } from '../as-form-control';

export const FormGroup = asFormControl(class extends Component {
	@bind
	_handleChange(event) {
		const name = this.props.name;
		let value = getControlValue(event.target);
		if (event.target.name) {
			if (value === undefined) {
				value = { ...value };
				delete value[event.target.name];
			}
			else {
				value = ({ ...this.props.value, [event.target.name]: value });
			}
		}
		this.props.onChange({ target: { name, value } });
	}

	@bind
	_linker(vnode) {
		if (isFormControl(vnode)) {
			const control = vnode.props || {};
			const { name, disabled: disabledAttr } = control;
			const { value, disabled: disabledProp } = this.props;
			return [{
				disabled: disabledProp || disabledAttr,
				[getControlEvent(vnode)]: this._handleChange,
				...controlValueToProp(control, value && (name == null ? value : value[name]))
			}];
		}
		return null;
	}

	render({ children, ...attrs }) {
		const c = toChildArray(children);
		const isRenderCallback = c && typeof c[0] === 'function';
		const params = { ...attrs, onChange: this._handleChange };
		return withProps(isRenderCallback ? [c[0](params)] : c, this._linker);
	}
});
