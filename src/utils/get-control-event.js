export function getControlEvent(node) {
	return typeof node.type === 'string' ? 'onInput' : 'onChange';
}
