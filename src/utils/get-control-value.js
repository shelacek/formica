export function getControlValue(control) {
	switch (control.type) {
		case 'checkbox':
			return control.checked;
		case 'number':
		case 'range':
			return control.value ? control.valueAsNumber : null;
		default:
			return control.value;
	}
}
