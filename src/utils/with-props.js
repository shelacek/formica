import { toChildArray, cloneElement } from 'preact';

export function withProps(children, mapper) {
	return toChildArray(children).map(vnode => {
		const mapping = mapper(vnode);
		if (mapping) {
			return cloneElement(vnode, ...mapping);
		}
		const c = vnode.props && toChildArray(vnode.props.children);
		return (c && c.length)
			? cloneElement(vnode, null, withProps(c, mapper))
			: vnode;
	});
}
