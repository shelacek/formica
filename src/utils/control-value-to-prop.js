export function controlValueToProp(control, value) {
	switch (value != null && control.type) {
		case 'checkbox':
			return { checked: value };
		case 'radio':
			return { checked: value === control.value };
		case 'number':
			return { value: isNaN(value) ? '' : `${value}` };
		case 'range':
			return { valueAsNumber: value };
		default:
			return { value };
	}
}
