import { FORM_CONTROL_SYMBOL } from '../internal/form-control-symbol';

export function isFormControl(node) {
	return node.type
		&& ((node.type === 'input'
		&& (!node.props || ['submit', 'button'].indexOf(node.props.type) === -1))
		|| ['textarea', 'select'].indexOf(node.type) !== -1
		|| node.type.type === FORM_CONTROL_SYMBOL);
}
